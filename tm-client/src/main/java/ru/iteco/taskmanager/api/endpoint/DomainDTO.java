
package ru.iteco.taskmanager.api.endpoint;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for domainDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="domainDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="project" type="{http://endpoint.api.taskmanager.iteco.ru/}projectDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="task" type="{http://endpoint.api.taskmanager.iteco.ru/}taskDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="user" type="{http://endpoint.api.taskmanager.iteco.ru/}userDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "domainDTO", propOrder = {
    "project",
    "task",
    "user"
})
public class DomainDTO {

    protected List<ProjectDTO> project;
    protected List<TaskDTO> task;
    protected List<UserDTO> user;

    /**
     * Gets the value of the project property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the project property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getProject().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ProjectDTO }
     * 
     * 
     */
    public List<ProjectDTO> getProject() {
        if (project == null) {
            project = new ArrayList<ProjectDTO>();
        }
        return this.project;
    }

    /**
     * Gets the value of the task property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the task property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTask().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TaskDTO }
     * 
     * 
     */
    public List<TaskDTO> getTask() {
        if (task == null) {
            task = new ArrayList<TaskDTO>();
        }
        return this.task;
    }

    /**
     * Gets the value of the user property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the user property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUser().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserDTO }
     * 
     * 
     */
    public List<UserDTO> getUser() {
        if (user == null) {
            user = new ArrayList<UserDTO>();
        }
        return this.user;
    }

}
