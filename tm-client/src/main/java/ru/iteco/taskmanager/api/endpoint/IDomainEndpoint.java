package ru.iteco.taskmanager.api.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.FaultAction;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2019-11-26T17:47:59.474+03:00
 * Generated source version: 3.2.7
 *
 */
@WebService(targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", name = "IDomainEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface IDomainEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonSaveXmlRequest", output = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonSaveXmlResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonSaveXml/Fault/Exception")})
    @RequestWrapper(localName = "jacksonSaveXml", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JacksonSaveXml")
    @ResponseWrapper(localName = "jacksonSaveXmlResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JacksonSaveXmlResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.DomainDTO jacksonSaveXml(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbSaveXmlRequest", output = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbSaveXmlResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbSaveXml/Fault/Exception")})
    @RequestWrapper(localName = "jaxbSaveXml", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JaxbSaveXml")
    @ResponseWrapper(localName = "jaxbSaveXmlResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JaxbSaveXmlResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.DomainDTO jaxbSaveXml(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonSaveJsonRequest", output = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonSaveJsonResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonSaveJson/Fault/Exception")})
    @RequestWrapper(localName = "jacksonSaveJson", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JacksonSaveJson")
    @ResponseWrapper(localName = "jacksonSaveJsonResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JacksonSaveJsonResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.DomainDTO jacksonSaveJson(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/saveDataRequest", output = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/saveDataResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/saveData/Fault/Exception")})
    @RequestWrapper(localName = "saveData", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.SaveData")
    @ResponseWrapper(localName = "saveDataResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.SaveDataResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.DomainDTO saveData(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/loadDataRequest", output = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/loadDataResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/loadData/Fault/Exception")})
    @RequestWrapper(localName = "loadData", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.LoadData")
    @ResponseWrapper(localName = "loadDataResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.LoadDataResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.DomainDTO loadData(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbSaveJsonRequest", output = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbSaveJsonResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbSaveJson/Fault/Exception")})
    @RequestWrapper(localName = "jaxbSaveJson", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JaxbSaveJson")
    @ResponseWrapper(localName = "jaxbSaveJsonResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JaxbSaveJsonResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.DomainDTO jaxbSaveJson(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbLoadXmlRequest", output = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbLoadXmlResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbLoadXml/Fault/Exception")})
    @RequestWrapper(localName = "jaxbLoadXml", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JaxbLoadXml")
    @ResponseWrapper(localName = "jaxbLoadXmlResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JaxbLoadXmlResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.DomainDTO jaxbLoadXml(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonLoadXmlRequest", output = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonLoadXmlResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonLoadXml/Fault/Exception")})
    @RequestWrapper(localName = "jacksonLoadXml", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JacksonLoadXml")
    @ResponseWrapper(localName = "jacksonLoadXmlResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JacksonLoadXmlResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.DomainDTO jacksonLoadXml(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbLoadJsonRequest", output = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbLoadJsonResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jaxbLoadJson/Fault/Exception")})
    @RequestWrapper(localName = "jaxbLoadJson", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JaxbLoadJson")
    @ResponseWrapper(localName = "jaxbLoadJsonResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JaxbLoadJsonResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.DomainDTO jaxbLoadJson(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    ) throws Exception_Exception;

    @WebMethod
    @Action(input = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonLoadJsonRequest", output = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonLoadJsonResponse", fault = {@FaultAction(className = Exception_Exception.class, value = "http://endpoint.api.taskmanager.iteco.ru/IDomainEndpoint/jacksonLoadJson/Fault/Exception")})
    @RequestWrapper(localName = "jacksonLoadJson", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JacksonLoadJson")
    @ResponseWrapper(localName = "jacksonLoadJsonResponse", targetNamespace = "http://endpoint.api.taskmanager.iteco.ru/", className = "ru.iteco.taskmanager.api.endpoint.JacksonLoadJsonResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.iteco.taskmanager.api.endpoint.DomainDTO jacksonLoadJson(
        @WebParam(name = "session", targetNamespace = "")
        ru.iteco.taskmanager.api.endpoint.SessionDTO session
    ) throws Exception_Exception;
}
