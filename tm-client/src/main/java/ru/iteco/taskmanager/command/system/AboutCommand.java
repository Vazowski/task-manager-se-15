package ru.iteco.taskmanager.command.system;

import javax.inject.Singleton;

import com.jcabi.manifests.Manifests;

import ru.iteco.taskmanager.command.AbstractCommand;

@Singleton
public class AboutCommand extends AbstractCommand {

    @Override
    public String command() {
	return "about";
    }

    @Override
    public String description() {
	return "  -  show information about programm";
    }

    @Override
    public void execute() throws Exception {
	System.out.println("Build version: " + Manifests.read("buildnumber"));
    }
}
