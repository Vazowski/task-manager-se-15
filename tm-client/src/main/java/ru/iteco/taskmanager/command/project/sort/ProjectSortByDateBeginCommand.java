package ru.iteco.taskmanager.command.project.sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.ProjectDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Singleton
public class ProjectSortByDateBeginCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private IProjectEndpoint projectEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
	return "project-sort-date-begin";
    }

    @Override
    public String description() {
	return "  -  find all project and sort them by date begin";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;
	@Nullable
	final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
	if (userDTO == null)
	    return;

	@Nullable
	final List<Project> tempList = ProjectDTOConvertUtil.DTOsToProjects(projectEndpoint.findAllProject(sessionDTO));

	@NotNull
	final Comparator<Project> compareByDateBegin = (Project o1, Project o2) -> o1.getDateBegin()
		.compareTo(o2.getDateBegin());
	Collections.sort(tempList, compareByDateBegin);

	for (int i = 0, j = 1; i < tempList.size(); i++) {
	    if (tempList.get(i).getOwnerId().equals(userDTO.getId())) {
		System.out.println("[Project " + (j++) + "]");
		System.out.println(tempList.get(i));
	    }
	}
    }

}
