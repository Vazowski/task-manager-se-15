package ru.iteco.taskmanager.command.project.find;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.Project;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.ProjectDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Singleton
public class ProjectFindAllCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private IProjectEndpoint projectEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
        return "project-find-all";
    }

    @Override
    public String description() {
        return "  -  find all project";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
        if (sessionDTO == null)
            return;
        @Nullable final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
        if (userDTO == null)
            return;

        @Nullable final List<Project> projectList = ProjectDTOConvertUtil
                .DTOsToProjects(projectEndpoint.findAllProject(sessionDTO));
        if (projectList == null) {
            System.out.println("No project exist");
            return;
        }

        for (int i = 0, j = 1; i < projectList.size(); i++) {
            if (projectList.get(i).getOwnerId().equals(userDTO.getId())) {
                System.out.println("[Project " + (j++) + "]");
                System.out.println("ID: " + projectList.get(i).getId());
                System.out.println("OwnerID: " + projectList.get(i).getOwnerId());
                System.out.println("Name: " + projectList.get(i).getName());
                System.out.println("Description: " + projectList.get(i).getDescription());
                System.out.println("DateCreated: " + projectList.get(i).getDateCreated());
                System.out.println("DateBegin: " + projectList.get(i).getDateBegin());
                System.out.println("DateEnd: " + projectList.get(i).getDateEnd());
                System.out.println("Status: " + projectList.get(i).getReadinessStatus().toString());
            }
        }
    }
}