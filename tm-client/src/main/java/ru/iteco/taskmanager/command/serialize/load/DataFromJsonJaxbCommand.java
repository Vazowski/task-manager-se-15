package ru.iteco.taskmanager.command.serialize.load;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IDomainEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Singleton
public class DataFromJsonJaxbCommand extends AbstractCommand {

    @Inject
    private IDomainEndpoint domainEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
	return "jaxb-from-json";
    }

    @Override
    public String description() {
	return "  -  load data from json via jaxb";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;

	if (domainEndpoint.jaxbLoadJson(sessionDTO) != null) {
	    sessionService.setSession(null);
	    System.out.println("Done");
	}
    }

}
