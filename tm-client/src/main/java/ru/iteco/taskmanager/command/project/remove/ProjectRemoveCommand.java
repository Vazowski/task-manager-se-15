package ru.iteco.taskmanager.command.project.remove;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.ProjectDTO;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Singleton
public class ProjectRemoveCommand extends AbstractCommand {

    @Inject
    private IProjectEndpoint projectEndpoint;
    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
        return "project-remove";
    }

    @Override
    public String description() {
        return "  -  remove one project";
    }

    @Override
    public void execute() throws Exception {
        @Nullable final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
        if (sessionDTO == null)
            return;
        @Nullable final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
        if (userDTO == null)
            return;

        System.out.print("Name of project: ");
        @NotNull final String inputName = scanner.nextLine();
        @Nullable
        ProjectDTO projectDTO = projectEndpoint.findProjectByName(sessionDTO, inputName);
        if (projectDTO == null) {
            System.out.println("No project with same name");
            return;
        }

        if (!projectDTO.getOwnerId().equals(userDTO.getId())) {
            System.out.println("You don't have permission");
            return;
        }
        projectEndpoint.removeProjectById(sessionDTO, projectDTO.getId());
        System.out.println("Done");
    }
}
