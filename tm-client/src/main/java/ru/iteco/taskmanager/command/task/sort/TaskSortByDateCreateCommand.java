package ru.iteco.taskmanager.command.task.sort;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.ITaskEndpoint;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.Task;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.TaskDTOConvertUtil;

@Singleton
public class TaskSortByDateCreateCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private ITaskEndpoint taskEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
	return "task-sort-date-create";
    }

    @Override
    public String description() {
	return "  -  find all task and sort them by date create";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;
	@Nullable
	final UserDTO userDTO = userEndpoint.findUserById(sessionDTO, sessionDTO.getUserId());
	if (userDTO == null)
	    return;

	@Nullable
	final List<Task> tempList = TaskDTOConvertUtil.DTOsToTasks(taskEndpoint.findAllTask(sessionDTO));
	@NotNull
	final Comparator<Task> compareByDateCreate = (Task o1, Task o2) -> o1.getDateCreated()
		.compareTo(o2.getDateCreated());
	Collections.sort(tempList, compareByDateCreate);

	for (int i = 0, j = 1; i < tempList.size(); i++) {
	    if (tempList.get(i).getOwnerId().equals(userDTO.getId())) {
		System.out.println("[Task " + (j++) + "]");
		System.out.println(tempList.get(i));
	    }
	}
    }

}
