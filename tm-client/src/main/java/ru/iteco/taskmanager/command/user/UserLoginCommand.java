package ru.iteco.taskmanager.command.user;

import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;

@Singleton
public class UserLoginCommand extends AbstractCommand {

    @Inject
    private ISessionService sessionService;
    @Inject
    private ISessionEndpoint sessionEndpoint;

    @Override
    public String command() {
	return "login";
    }

    @Override
    public String description() {
	return "  -  login user";
    }

    @Override
    public void execute() throws Exception {
	while (true) {
	    System.out.print("Login: ");
	    @NotNull
	    final String login = scanner.nextLine();
	    System.out.print("Password: ");
	    @NotNull
	    final String password = scanner.nextLine();

	    @Nullable
	    final SessionDTO sessionDTO = sessionEndpoint.signSession(login, password);
	    if (sessionDTO == null) {
		System.out.println("Username or password was incorrect. Try again");
		continue;
	    }
	    //sessionDTO.setId(UUID.randomUUID().toString());
	    sessionService.setSession(SessionDTOConvertUtil.DTOToSession(sessionDTO));
	    sessionEndpoint.persistSession(sessionDTO);
	    System.out.println("Done");
	    break;
	}
    }
}
