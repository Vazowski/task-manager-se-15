package ru.iteco.taskmanager.command.user;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.api.ISessionService;
import ru.iteco.taskmanager.api.endpoint.IUserEndpoint;
import ru.iteco.taskmanager.api.endpoint.RoleType;
import ru.iteco.taskmanager.api.endpoint.SessionDTO;
import ru.iteco.taskmanager.api.endpoint.User;
import ru.iteco.taskmanager.api.endpoint.UserDTO;
import ru.iteco.taskmanager.command.AbstractCommand;
import ru.iteco.taskmanager.util.HashUtil;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.UserDTOConvertUtil;

@Singleton
public class UserCreateCommand extends AbstractCommand {

    @Inject
    private IUserEndpoint userEndpoint;
    @Inject
    private ISessionService sessionService;

    @Override
    public String command() {
	return "user-create";
    }

    @Override
    public String description() {
	return "  -  create new user";
    }

    @Override
    public void execute() throws Exception {
	@Nullable
	final SessionDTO sessionDTO = SessionDTOConvertUtil.sessionToDTO(sessionService.getSession());
	if (sessionDTO == null)
	    return;

	System.out.print("Login: ");
	@NotNull
	final String login = scanner.nextLine();
	@Nullable
	final List<User> userList = UserDTOConvertUtil.DTOsToUsers(userEndpoint.findAllUser(sessionDTO));

	for (User user : userList) {
	    if (user.getLogin().equals(login)) {
		System.out.println("Login exist");
		return;
	    }
	}

	System.out.print("Password: ");
	@NotNull
	final String password = scanner.nextLine();
	System.out.print("Email: ");
	@NotNull
	final String email = scanner.nextLine();
	System.out.print("First name: ");
	@NotNull
	final String firstName = scanner.nextLine();
	System.out.print("Last name: ");
	@NotNull
	final String lastName = scanner.nextLine();
	System.out.print("Middle name: ");
	@NotNull
	final String middleName = scanner.nextLine();
	System.out.print("Phone: ");
	@NotNull
	final String phone = scanner.nextLine();

	@Nullable
	final UserDTO userDTO = new UserDTO();
	userDTO.setId(UUID.randomUUID().toString());
	userDTO.setEmail(email);
	userDTO.setFirstName(firstName);
	userDTO.setLastName(lastName);
	userDTO.setLogin(login);
	userDTO.setLogin(middleName);
	userDTO.setLogin(HashUtil.getHash(password));
	userDTO.setLogin(phone);
	userDTO.setRoleType(RoleType.USER);
	userEndpoint.persist(sessionDTO, userDTO);
	System.out.println("Done");
    }

    @NotNull
    @Override
    public List<String> getRoles() {
	return new ArrayList<String>() {
	    {
		add("Administrator");
	    }
	};
    }
}
