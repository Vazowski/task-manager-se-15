package ru.iteco.taskmanager;

import java.util.List;

import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ru.iteco.taskmanager.api.endpoint.*;
import ru.iteco.taskmanager.endpoint.ProjectEndpointService;
import ru.iteco.taskmanager.endpoint.SessionEndpointService;

public class ProjectEndpointTest {

    private IProjectEndpoint projectEndpoint = new ProjectEndpointService().getProjectEndpointPort();
    private ISessionEndpoint sessionEndpoint = new SessionEndpointService().getSessionEndpointPort();

    private SessionDTO sessionDTO;
    private ProjectDTO tempProject;

    @Before
    public void connect() {
        sessionDTO = sessionEndpoint.signSession("test", "1234");
        sessionEndpoint.persistSession(sessionDTO);

        tempProject = new ProjectDTO();
        tempProject.setId("1");
        tempProject.setName("Test project");
        tempProject.setDateCreated("10.10.2020");
        tempProject.setDateBegin("10.01.2010");
        tempProject.setDateEnd("10.01.2030");
        tempProject.setDescription("This is description for test project");
        tempProject.setType("Project");
        tempProject.setOwnerId("555");
        tempProject.setReadinessStatus(ReadinessStatus.PLANNED);
        projectEndpoint.projectPersist(sessionDTO, tempProject);
        tempProject.setId("2");
        tempProject.setName("Test project for multi projects");
        projectEndpoint.projectPersist(sessionDTO, tempProject);
    }

    @Test
    public void projectExist() {
        @Nullable
        ProjectDTO testProject = new ProjectDTO();
        testProject.setId("1");
        @Nullable
        final ProjectDTO projectDTO = projectEndpoint.findProjectById(sessionDTO, testProject.getId());
        Assert.assertEquals(projectDTO.getId(), testProject.getId());
    }

    @Test
    public void projectsExists() {
        final List<ProjectDTO> projectsList = projectEndpoint.findAllProject(sessionDTO);
        Assert.assertEquals(projectsList.size(), 2);
    }

    @After
    public void disconnect() {
        projectEndpoint.removeAllProject(sessionDTO);
    }
}
