package ru.iteco.taskmanager.service;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import org.jetbrains.annotations.Nullable;

import lombok.SneakyThrows;
import ru.iteco.taskmanager.util.DatabaseUtil;

public class AbstractService {

    @Nullable
    EntityManagerFactory entityManagerFactory;
    @Nullable
    EntityManager entityManager;

    @SneakyThrows
    public AbstractService() {
	entityManagerFactory = DatabaseUtil.factory();
    }
}
