package ru.iteco.taskmanager.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

import javax.inject.Singleton;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;

import ru.iteco.taskmanager.api.service.IDomainService;
import ru.iteco.taskmanager.dto.DomainDTO;

@Singleton
public class DomainService implements IDomainService {

    @Nullable
    public DomainDTO saveData(@NotNull final DomainDTO domain) throws Exception {
	@NotNull
	final File file = new File("dump.bin");
	@NotNull
	final FileOutputStream fileOutputStream = new FileOutputStream(file);
	@NotNull
	final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
	objectOutputStream.writeObject(domain);
	objectOutputStream.close();
	fileOutputStream.close();
	return domain;
    }

    @Nullable
    public DomainDTO loadData() throws Exception {
	File f = new File("dump.bin");
	if (!f.exists()) {
	    System.out.println("File dump.bin not found");
	    return null;
	}

	@NotNull
	final File file = new File("dump.bin");
	@NotNull
	final FileInputStream fileInputStream = new FileInputStream(file);
	@NotNull
	final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
	@Nullable
	final DomainDTO domain = (DomainDTO) objectInputStream.readObject();
	objectInputStream.close();
	fileInputStream.close();
	return domain;
    }

    @Nullable
    public DomainDTO jaxbSaveXml(@NotNull final DomainDTO domain) throws Exception {
	try {
	    @Nullable
	    final JAXBContext jaxbContext = JAXBContext.newInstance(domain.getClass());
	    @Nullable
	    final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

	    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	    jaxbMarshaller.marshal(domain, new File("jaxb.xml"));
	} catch (JAXBException e) {
	    e.printStackTrace();
	    return null;
	}
	return domain;
    }

    @Nullable
    public DomainDTO jaxbLoadXml() {
	@Nullable
	DomainDTO domain = new DomainDTO();
	try {
	    @Nullable
	    final JAXBContext jaxbContext = JAXBContext.newInstance(domain.getClass());

	    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	    domain = (DomainDTO) jaxbUnmarshaller.unmarshal(new File("jaxb.xml"));
	} catch (JAXBException e) {
	    e.printStackTrace();
	}
	return domain;
    }

    @Nullable
    public DomainDTO jaxbSaveJson(@NotNull final DomainDTO domain) throws Exception {
	final Map<String, Object> properties = new HashMap<>();
	final Class[] classes = new Class[] { DomainDTO.class };
	System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
	properties.put("eclipselink-media-type", "application/json");
	@Nullable
	final JAXBContext jaxbContext = JAXBContext.newInstance(classes, properties);
	@Nullable
	final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
	jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
	jaxbMarshaller.marshal(domain, new File("jaxb.json"));
	return domain;
    }

    @Nullable
    public DomainDTO jaxbLoadJson() throws Exception {
	final Map<String, Object> properties = new HashMap<>();
	final Class[] classes = new Class[] { DomainDTO.class };
	System.setProperty("javax.xml.bind.context.factory", "org.eclipse.persistence.jaxb.JAXBContextFactory");
	properties.put("eclipselink-media-type", "application/json");
	@Nullable
	final JAXBContext jaxbContext = JAXBContext.newInstance(classes, properties);
	@Nullable
	final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	return (DomainDTO) jaxbUnmarshaller.unmarshal(new File("jaxb.json"));
    }

    @Nullable
    public DomainDTO jacksonSaveXml(@NotNull final DomainDTO domain) throws Exception {
	@NotNull
	final ObjectMapper mapper = new XmlMapper();
	@NotNull
	final String result = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
	FileWriter writer = new FileWriter("jackson.xml");
	writer.write(result);
	writer.flush();
	writer.close();
	return domain;
    }

    @Nullable
    public DomainDTO jacksonLoadXml() throws Exception {
	XmlMapper xmlMapper = new XmlMapper();
	String readContent = new String(Files.readAllBytes(Paths.get("jackson.xml")));
	DomainDTO domain = xmlMapper.readValue(readContent, DomainDTO.class);
	return domain;
    }

    @Nullable
    public DomainDTO jacksonSaveJson(@NotNull final DomainDTO domain) throws Exception {
	@NotNull
	final ObjectMapper mapper = new ObjectMapper();
	mapper.writerWithDefaultPrettyPrinter().writeValue(new File("jackson.json"), domain);
	return domain;
    }

    @Nullable
    public DomainDTO jacksonLoadJson() throws Exception {
	ObjectMapper mapper = new ObjectMapper();
	DomainDTO domain = mapper.readValue(new File("jackson.json"), DomainDTO.class);
	return domain;
    }
}
