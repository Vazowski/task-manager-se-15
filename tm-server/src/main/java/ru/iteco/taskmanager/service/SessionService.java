package ru.iteco.taskmanager.service;

import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.ISessionService;
import ru.iteco.taskmanager.entity.Session;
import ru.iteco.taskmanager.repository.SessionRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

@Singleton
@NoArgsConstructor
public class SessionService extends AbstractService implements ISessionService {

    @Nullable
    private SessionRepository sessionRepository;

    public void merge(@Nullable final Session session) {
	if (session == null)
	    return;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    sessionRepository = new SessionRepository(entityManager);
	    entityManager.getTransaction().begin();
	    sessionRepository.merge(session);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }

    public void persist(@Nullable final Session session) {
	if (session == null)
	    return;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    sessionRepository = new SessionRepository(entityManager);
	    entityManager.getTransaction().begin();
	    sessionRepository.persist(session);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }

    @Nullable
    public Session findById(@Nullable final String id) {
	@Nullable
	Session session = null;
	if (id == null)
	    return null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    sessionRepository = new SessionRepository(entityManager);
	    entityManager.getTransaction().begin();
	    session = sessionRepository.findById(id);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return session;
    }

    public void remove(@NotNull final String id) {
	if (id == null)
	    return;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    sessionRepository = new SessionRepository(entityManager);
	    entityManager.getTransaction().begin();
	    sessionRepository.remove(id);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }
}
