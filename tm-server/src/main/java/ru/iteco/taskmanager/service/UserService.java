package ru.iteco.taskmanager.service;

import java.util.List;

import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.entity.User;
import ru.iteco.taskmanager.repository.UserRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

@Singleton
@NoArgsConstructor
public class UserService extends AbstractService implements IUserService {

    @Nullable
    private UserRepository userRepository;

    public void merge(@NotNull final User user) {
	if (user == null)
	    return;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    userRepository = new UserRepository(entityManager);
	    entityManager.getTransaction().begin();
	    userRepository.merge(user);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }

    public void persist(@NotNull final User user) {
	if (user == null)
	    return;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    userRepository = new UserRepository(entityManager);
	    entityManager.getTransaction().begin();
	    userRepository.persist(user);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }

    @Nullable
    public User findById(@NotNull final String id) {
	@Nullable
	User user = null;
	if (id == null)
	    return null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    userRepository = new UserRepository(entityManager);
	    entityManager.getTransaction().begin();
	    user = userRepository.findById(id);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return user;
    }

    @Nullable
    public User findByLogin(@NotNull final String login) {
	@Nullable
	User user = null;
	if (login == null)
	    return null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    userRepository = new UserRepository(entityManager);
	    entityManager.getTransaction().begin();
	    user = userRepository.findByLogin(login);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return user;
    }

    @Nullable
    public List<User> findAll() {
	@Nullable
	List<User> userList = null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    userRepository = new UserRepository(entityManager);
	    entityManager.getTransaction().begin();
	    userList = userRepository.findAll();
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return userList;
    }
}