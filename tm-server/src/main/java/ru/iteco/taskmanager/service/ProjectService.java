package ru.iteco.taskmanager.service;

import java.util.List;

import javax.inject.Singleton;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.NoArgsConstructor;
import ru.iteco.taskmanager.api.service.IProjectService;
import ru.iteco.taskmanager.entity.Project;
import ru.iteco.taskmanager.repository.ProjectRepository;
import ru.iteco.taskmanager.util.DatabaseUtil;

@Singleton
@NoArgsConstructor
public final class ProjectService extends AbstractService implements IProjectService {

    @Nullable
    private ProjectRepository projectRepository;

    public void merge(@NotNull final Project project) {
	if (project == null)
	    return;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    projectRepository = new ProjectRepository(entityManager);
	    entityManager.getTransaction().begin();
	    projectRepository.merge(project);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }

    public void persist(@NotNull final Project project) {
	if (project == null)
	    return;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    projectRepository = new ProjectRepository(entityManager);
	    entityManager.getTransaction().begin();
	    projectRepository.persist(project);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }

    @Nullable
    public Project findById(@NotNull final String id) {
	@Nullable
	Project project = null;
	if (id == null)
	    return null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    projectRepository = new ProjectRepository(entityManager);
	    entityManager.getTransaction().begin();
	    project = projectRepository.findById(id);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return project;
    }

    @Nullable
    public Project findByName(@NotNull final String name) {
	@Nullable
	Project project = null;
	if (name == null)
	    return null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    projectRepository = new ProjectRepository(entityManager);
	    entityManager.getTransaction().begin();
	    project = projectRepository.findByName(name);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return project;
    }

    @Nullable
    public List<Project> findAll() {
	@Nullable
	List<Project> projectList = null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    projectRepository = new ProjectRepository(entityManager);
	    entityManager.getTransaction().begin();
	    projectList = projectRepository.findAll();
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return projectList;
    }

    @Nullable
    public List<Project> findAllByOwnerId(@NotNull final String ownerId) {
	@Nullable
	List<Project> projectList = null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    projectRepository = new ProjectRepository(entityManager);
	    entityManager.getTransaction().begin();
	    projectList = projectRepository.findAllByOwnerId(ownerId);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return projectList;
    }

    @Nullable
    public List<Project> findAllByPartOfName(@NotNull final String partOfName) {
	@Nullable
	List<Project> projectList = null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    projectRepository = new ProjectRepository(entityManager);
	    entityManager.getTransaction().begin();
	    projectList = projectRepository.findAllByPartOfName(partOfName);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return projectList;
    }

    @Nullable
    public List<Project> findAllByPartOfDescription(@NotNull final String partOfDescription) {
	@Nullable
	List<Project> projectList = null;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    projectRepository = new ProjectRepository(entityManager);
	    entityManager.getTransaction().begin();
	    projectList = projectRepository.findAllByPartOfDescription(partOfDescription);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
	return projectList;
    }

    public void remove(@NotNull final String id) {
	if (id == null)
	    return;
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    projectRepository = new ProjectRepository(entityManager);
	    entityManager.getTransaction().begin();
	    projectRepository.remove(id);
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }

    public void removeAll() {
	try {
	    entityManagerFactory = DatabaseUtil.factory();
	    entityManager = entityManagerFactory.createEntityManager();
	    projectRepository = new ProjectRepository(entityManager);
	    entityManager.getTransaction().begin();
	    projectRepository.removeAll();
	    entityManager.getTransaction().commit();
	} catch (Exception e) {
	    entityManager.getTransaction().rollback();
	    e.printStackTrace();
	} finally {
	    entityManager.close();
	}
    }
}
