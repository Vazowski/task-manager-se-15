package ru.iteco.taskmanager;

import javax.enterprise.inject.se.SeContainerInitializer;

import ru.iteco.taskmanager.bootstrap.Bootstrap;

public class App {
    public static void main(final String[] args) {
	SeContainerInitializer.newInstance().addPackages(App.class).initialize().select(Bootstrap.class).get().init();
    }
}
