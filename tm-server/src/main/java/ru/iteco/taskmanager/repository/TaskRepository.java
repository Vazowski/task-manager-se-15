package ru.iteco.taskmanager.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.entity.Task;

public class TaskRepository {

    @NotNull
    private final EntityManager entityManager;

    public TaskRepository(@Nullable final EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    public void merge(@NotNull final Task task) {
	entityManager.merge(task);
    }

    public void persist(@NotNull final Task task) {
	entityManager.persist(task);
    }

    @NotNull
    public Task findById(@NotNull final String id) {
	return entityManager.find(Task.class, id);
    }

    @NotNull
    public Task findByName(@NotNull final String name) {
	return entityManager.createQuery("SELECT a FROM Task a WHERE a.name=:name", Task.class)
		.setParameter("name", name).getSingleResult();
    }

    @NotNull
    public Task findByProjectId(@NotNull final String projectId) {
	return entityManager.createQuery("SELECT a FROM Task a WHERE a.project.id=:projectId", Task.class)
		.setParameter("projectId", projectId).getSingleResult();
    }

    @NotNull
    public List<Task> findAll() {
	return entityManager.createQuery("SELECT a FROM Task a", Task.class).getResultList();
    }

    @NotNull
    public List<Task> findAllByOwnerId(@NotNull final String ownerId) {
	return entityManager.createQuery("SELECT a FROM Task a WHERE a.user.id=:ownerId", Task.class)
		.setParameter("ownerId", ownerId).getResultList();
    }

    @NotNull
    public List<Task> findAllByPartOfName(@NotNull final String partOfName) {
	return entityManager.createQuery("SELECT a FROM Task a WHERE name LIKE %:partOfName%", Task.class)
		.setParameter("partOfName", partOfName).getResultList();
    }

    @NotNull
    public List<Task> findAllByPartOfDescription(@NotNull final String partOfDescription) {
	return entityManager.createQuery("SELECT a FROM Task a WHERE description LIKE %:partOfDescription%", Task.class)
		.setParameter("partOfDescription", partOfDescription).getResultList();
    }

    public void remove(@NotNull final String id) {
	entityManager.remove(entityManager.find(Task.class, id));
    }

    public void removeAll() {
	for (Task task : findAll())
	    entityManager.remove(task);
    }
}
