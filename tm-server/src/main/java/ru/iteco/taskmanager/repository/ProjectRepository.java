package ru.iteco.taskmanager.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.entity.Project;

public class ProjectRepository {

    @NotNull
    private final EntityManager entityManager;

    public ProjectRepository(@Nullable final EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    public void merge(@NotNull final Project project) {
	entityManager.merge(project);
    }

    public void persist(@NotNull final Project project) {
	entityManager.persist(project);
    }

    @NotNull
    public Project findById(@NotNull final String id) {
	return entityManager.find(Project.class, id);
    }

    @NotNull
    public Project findByName(@NotNull final String name) {
	return entityManager.createQuery("SELECT a FROM Project a WHERE a.name=:name", Project.class)
		.setParameter("name", name).getSingleResult();
    }

    @NotNull
    public List<Project> findAll() {
	return entityManager.createQuery("SELECT a FROM Project a", Project.class).getResultList();
    }

    @NotNull
    public List<Project> findAllByOwnerId(@NotNull final String ownerId) {
	return entityManager.createQuery("SELECT a FROM Project a WHERE a.user.id=:ownerId", Project.class)
		.setParameter("ownerId", ownerId).getResultList();
    }

    @NotNull
    public List<Project> findAllByPartOfName(@NotNull final String partOfName) {
	return entityManager.createQuery("SELECT a FROM Project a WHERE name LIKE %:partOfName%", Project.class)
		.setParameter("partOfName", partOfName).getResultList();
    }

    @NotNull
    public List<Project> findAllByPartOfDescription(@NotNull final String partOfDescription) {
	return entityManager
		.createQuery("SELECT a FROM Project a WHERE description LIKE %:partOfDescription%", Project.class)
		.setParameter("partOfDescription", partOfDescription).getResultList();
    }

    public void remove(@NotNull final String id) {
	entityManager.remove(entityManager.find(Project.class, id));
    }

    public void removeAll() {
	for (Project project : findAll())
	    entityManager.remove(project);
    }
}
