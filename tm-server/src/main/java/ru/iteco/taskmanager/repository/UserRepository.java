package ru.iteco.taskmanager.repository;

import java.util.List;

import javax.persistence.EntityManager;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import ru.iteco.taskmanager.entity.User;

public class UserRepository {

    @NotNull
    private final EntityManager entityManager;

    public UserRepository(@Nullable final EntityManager entityManager) {
	this.entityManager = entityManager;
    }

    public void merge(@NotNull final User user) {
	entityManager.merge(user);
    }

    public void persist(@NotNull final User user) {
	entityManager.persist(user);
    }

    @NotNull
    public User findById(@NotNull final String id) {
	return entityManager.find(User.class, id);
    }

    @NotNull
    public User findByLogin(@NotNull final String login) {
	return entityManager.createQuery("SELECT a FROM User a WHERE a.login=:login", User.class)
		.setParameter("login", login).getSingleResult();
    }

    @NotNull
    public List<User> findAll() {
	return entityManager.createQuery("SELECT a FROM User a", User.class).getResultList();
    }
}