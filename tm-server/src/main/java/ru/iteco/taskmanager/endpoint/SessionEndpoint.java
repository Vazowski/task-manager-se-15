package ru.iteco.taskmanager.endpoint;

import java.util.Date;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.ISessionEndpoint;
import ru.iteco.taskmanager.api.service.ISessionService;
import ru.iteco.taskmanager.api.service.IUserService;
import ru.iteco.taskmanager.dto.SessionDTO;
import ru.iteco.taskmanager.dto.UserDTO;
import ru.iteco.taskmanager.util.DateUtil;
import ru.iteco.taskmanager.util.HashUtil;
import ru.iteco.taskmanager.util.SignatureUtil;
import ru.iteco.taskmanager.util.convert.SessionDTOConvertUtil;
import ru.iteco.taskmanager.util.convert.UserDTOConvertUtil;

@Getter
@Setter
@Singleton
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.ISessionEndpoint")
public class SessionEndpoint implements ISessionEndpoint {

    @Inject
    private ISessionService sessionService;
    @Inject
    private IUserService userService;

    @WebMethod
    public void mergeSession(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
	sessionService.merge(SessionDTOConvertUtil.DTOToSession(sessionDTO));
    }

    @WebMethod
    public void persistSession(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
	sessionService.persist(SessionDTOConvertUtil.DTOToSession(sessionDTO));
    }

    @WebMethod
    @Nullable
    public SessionDTO signSession(@WebParam(name = "login") @Nullable final String login,
	    @WebParam(name = "password") @Nullable final String password) {
	@NotNull
	final SessionDTO sessionDTO = new SessionDTO();
	@Nullable
	final UserDTO user = UserDTOConvertUtil.userToDTO(userService.findByLogin(login));
	if (!user.getPasswordHash().equals(HashUtil.getHash(password)))
	    return null;

	@NotNull
	final String userId = user.getId();
	sessionDTO.setUserId(userId);
	@NotNull
	final String timeStamp = DateUtil.getDate(new Date().toString());
	sessionDTO.setTimestamp(timeStamp);
	sessionDTO.setSignature(SignatureUtil.sign(sessionDTO));
	return sessionDTO;
    }

    @WebMethod
    @Nullable
    public SessionDTO findById(@WebParam(name = "session") @Nullable final String id) {
	return SessionDTOConvertUtil.sessionToDTO(sessionService.findById(id));
    }

    @WebMethod
    public void removeSession(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
	sessionService.remove(sessionDTO.getId());
    }
}
