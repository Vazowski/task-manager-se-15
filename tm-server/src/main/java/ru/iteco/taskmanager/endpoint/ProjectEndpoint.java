package ru.iteco.taskmanager.endpoint;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import org.jetbrains.annotations.Nullable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.api.endpoint.IProjectEndpoint;
import ru.iteco.taskmanager.api.service.IProjectService;
import ru.iteco.taskmanager.dto.ProjectDTO;
import ru.iteco.taskmanager.dto.SessionDTO;
import ru.iteco.taskmanager.util.SignatureUtil;
import ru.iteco.taskmanager.util.convert.ProjectDTOConvertUtil;

@Getter
@Setter
@Singleton
@NoArgsConstructor
@WebService(endpointInterface = "ru.iteco.taskmanager.api.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    @Inject
    private IProjectService projectService;

    @WebMethod
    public void projectMerge(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "project") @Nullable final ProjectDTO projectDTO) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	projectService.merge(ProjectDTOConvertUtil.DTOToProject(projectDTO));
    }

    @WebMethod
    public void projectPersist(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "project") @Nullable final ProjectDTO projectDTO) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	projectService.persist(ProjectDTOConvertUtil.DTOToProject(projectDTO));
    }

    @WebMethod
    public @Nullable ProjectDTO findProjectById(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "id") @Nullable String id) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil.projectToDTO(projectService.findById(id));
    }

    @WebMethod
    public @Nullable ProjectDTO findProjectByName(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "projectName") @Nullable String projectName) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil.projectToDTO(projectService.findByName(projectName));
    }

    @WebMethod
    public @Nullable List<ProjectDTO> findAllProject(
	    @WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil.projectsToDTO(projectService.findAll());
    }

    @WebMethod
    public @Nullable List<ProjectDTO> findAllProjectByOwnerId(
	    @WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "ownerId") @Nullable final String ownerId) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil.projectsToDTO(projectService.findAllByOwnerId(ownerId));
    }

    @WebMethod
    public @Nullable List<ProjectDTO> findAllProjectByPartOfName(
	    @WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "partOfName") @Nullable final String partOfName) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil.projectsToDTO(projectService.findAllByPartOfName(partOfName));
    }

    @WebMethod
    public @Nullable List<ProjectDTO> findAllProjectByPartOfDescription(
	    @WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "partOfDescription") @Nullable final String partOfDescription) {
	SignatureUtil.validate(sessionDTO);
	return ProjectDTOConvertUtil.projectsToDTO(projectService.findAllByPartOfDescription(partOfDescription));
    }

    @WebMethod
    public void removeProjectById(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO,
	    @WebParam(name = "id") @Nullable String id) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	projectService.remove(id);
    }

    @WebMethod
    public void removeAllProject(@WebParam(name = "session") @Nullable final SessionDTO sessionDTO) {
	if (SignatureUtil.validate(sessionDTO) == null)
	    return;
	projectService.removeAll();
    }
}
