package ru.iteco.taskmanager.dto;

import org.jetbrains.annotations.NotNull;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.iteco.taskmanager.enumerate.RoleType;

@Getter
@Setter
@NoArgsConstructor
public final class UserDTO extends AbstractDTO {

    @NotNull
    private String email;
    @NotNull
    private String firstName;
    @NotNull
    private String lastName;
    @NotNull
    private String login;
    @NotNull
    private String middleName;
    @NotNull
    private String passwordHash;
    @NotNull
    private String phone;
    @NotNull
    private RoleType roleType;

    public UserDTO(@NotNull String id, @NotNull String email, @NotNull String firstName, @NotNull String lastName,
	    @NotNull String login, @NotNull String middleName, @NotNull String passwordHash, @NotNull String phone,
	    @NotNull RoleType roleType) {
	this.id = id;
	this.email = email;
	this.firstName = firstName;
	this.lastName = lastName;
	this.login = login;
	this.middleName = middleName;
	this.passwordHash = passwordHash;
	this.phone = phone;
	this.roleType = roleType;
    }
}
